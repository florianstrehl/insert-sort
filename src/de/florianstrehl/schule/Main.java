package de.florianstrehl.schule;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.List;

import java.util.concurrent.TimeUnit;
public class Main {
    //17,3,28,19,11,5,7,19,10,4
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("1. Geschwindigkeitstest (2.000.000.000)\n" +
                "2. Vorführung");
        System.out.print("Modus: ");
        int mode = scanner.nextInt();
        int[] result = {};
        String list = scanner.next();
        List<Integer> listInput = new ArrayList<Integer>();
        for (String s: list.split(",")) { //Eingabe am Komma trennen
            listInput.add(Integer.parseInt(s));
        }
        int[] arrayInput = listInput.stream().mapToInt(i -> i).toArray(); //List<Integer> nach int[] schreiben
        if (mode == 1) {
            long before = System.currentTimeMillis(); //Zeit vor der Berechnung
            for(int i = 0; i < 2000000000; i++) {
                result = BaseSort(arrayInput.clone()); /*Zusätzliche Zeit und Arbeitsspeicher durch clone()
                clone() dupliziert das Array an einen neuen Platz im Stack -> Frische Liste bei jedem Durchlauf
                */
            }
            System.out.format("Zeit: %dms\n", System.currentTimeMillis() - before); //Ausgabe der Dauer
        } else {
            try {
                result = DemoSort(arrayInput);
            } catch (InterruptedException e) { //Im Demo-Modus wird nach jedem Schritt pausiert. Dies kann eine InterruptedException werfen.
                e.printStackTrace();
            }
        }
        System.out.println(Arrays.toString(result));
    }

    /**
     * Dieser Teil ist der eigentliche Algorithmus
     * @param list
     * @return
     */
    public static int[] BaseSort(int[] list) {
        for(int i = 1; i < list.length; i++) { //Start beim zweiten Element des Arrays
            int x = i - 1; //Angepasster Index (Eins vor dem Ausgangswert)
            int datum = list[i]; //Zu betrachtender Wert
            while (x >= 0 && list[x] > datum) { //Solange der Index größer oder gleich 0, und das aktuelle Element größer als der zu betrachtender Wert
                list[x + 1] = list[x]; //Den Wert hinter list[x] mit dem Wert von list[x] überschreiben ([1,2,x,4] -> [1,2,x,x])
                x--; //Den angepassten Index um eins reduzieren
            }
            list[x + 1] = datum; //Den Wert hinter list[x] mit dem alten Wert von list[i] überschreiben
        }
        return list; //Die sortierte Liste zurückgeben
    }

    /**
     * Dieser Teil dient nur der visuellen Veranschaulichung für den Nutzer und der Zählung.
     * Bitte diesen Teil NICHT als Anleitung für den Insert-Sort Algorithmus nutzen.
     * @param list
     * @return
     * @throws InterruptedException
     */
    public static int[] DemoSort(int[] list) throws InterruptedException {
        int compareIndex = 0;
        int compareValue = 0;
        int swap = 0;
        int[] oldList = {};
        int oldValue = list[0];
        for(int i = 1; i < list.length; i++) {
            int x = i - 1;
            int datum = list[i];
            oldList = list.clone();
            while (true) {
                boolean xPositive = x >= 0;
                compareIndex++;
                System.out.format("%d. Vergleich (Index) %d >= 0: %s\n", compareIndex, x, xPositive);
                if (xPositive) {
                    boolean listBiggerDatum = list[x] > datum;

                    compareValue++;
                    System.out.format("%d. Vergleich (Wert) %d > %d: %s\n", compareValue, list[x], datum, listBiggerDatum);
                    if (listBiggerDatum) {
                        list[x + 1] = list[x];
                        x--;
                        if (x >= 0){
                            oldValue = list[x];
                        }

                    } else {
                        break;
                    }

                } else {
                    break;
                }
            }
            list[x + 1] = datum;
            boolean equal = true;
            for(int n = 0; n < list.length; n++) {
                if (list[n] != oldList[n]) {
                    equal = false;
                }
            }
            if (!equal) {
                swap++;
                if(swap == 1) {
                    System.out.format("%d. Vertauschung: %d vor %d\n", swap, datum, oldValue);
                } else {
                    System.out.format("%d. Vertauschung: %d hinter %d\n", swap, datum, oldValue);
                }
                System.out.format("\tNeue Liste: %s\n", Arrays.toString(list));
            }
            TimeUnit.SECONDS.sleep(3); //Der Wert kann ein beliebiger long-Wert sein

        }
        System.out.format("Vertauschungen:\t\t\t%d\n" +
                "Vergleiche (Index):\t\t%d\n" +
                "Vergleiche (Werte):\t\t%d\n" +
                "Vergleiche (Gesamt):\t%d\n", swap, compareIndex, compareValue, compareIndex + compareValue);
        return list;
    }
}
